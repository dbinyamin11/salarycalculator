package il.ac.hit.salarycalculator.test;

import static org.junit.Assert.*;

import il.ac.hit.salarycalculator.model.Shift;
import org.junit.Test;
import org.junit.Before;

public class ShiftTest {
    Shift shift = new Shift("test");

    @Before
    public void setUp() throws Exception {
        shift.setBegin("08:00");
        shift.setEnd("20:00");
        shift.setSalaryPerHour(33.4);
    }

    @Test
    public void getTotalSalary() {
        double exptcted = (double)12*33.4;
       assertEquals( exptcted,shift.getTotalSalary(),0.0002);
    }

    @Test
    public void TestBegin()
    {
        assertEquals("08:00",shift.getBegin());
    }

    @Test
    public void TestEnd()
    {
        assertEquals("20:00",shift.getEnd());
    }
}