package il.ac.hit.salarycalculator.test;

import il.ac.hit.salarycalculator.model.UserSetting;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserSettingTest {

    UserSetting userSetting = new UserSetting();
    @org.junit.Before
    public void setUp() throws Exception {
        userSetting.setId("123");
        userSetting.setSalary(32.2);
        userSetting.setUserName("test");
    }

    @Test
    public void getUserName() {
        assertEquals("test",userSetting.getUserName());
    }

    @Test
    public void getId() {
        assertEquals("123",userSetting.getId());
    }

    @Test
    public void getSalary() {
        assertEquals(32.2,userSetting.getSalary(),0);
    }
}