package il.ac.hit.salarycalculator.test;

import il.ac.hit.salarycalculator.viewmodel.ManagementViewModel;
import org.junit.Test;

import static org.junit.Assert.*;

public class ManagementViewModelTest {

    @Test
    public void getInstance() {
        assertNotNull(ManagementViewModel.getInstance());
    }
}