package il.ac.hit.salarycalculator.test;

import il.ac.hit.salarycalculator.model.DataBaseQuery;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;

public class DataBaseQueryTest {
    DataBaseQuery db = DataBaseQuery.getInstance();
    String nameOfTable = "Test";

    @After
    public void tearDown() throws Exception {
        db.dropTable(nameOfTable);
    }

    @Test
    public void insertValue() throws Exception {
        assertEquals(true, db.createTableIfNotExist(nameOfTable, "Username", "varchar(300)"));
    }

    @Test
    public void queryValidate()
    {
        try {
            assertEquals(true, db.query("SELECT * FROM TEST "));
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void StringConcat()
    {
        assertEquals("(check check)",db.stringsConcat("check","check"));
    }
}