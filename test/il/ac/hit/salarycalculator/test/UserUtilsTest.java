package il.ac.hit.salarycalculator.test;

import il.ac.hit.salarycalculator.model.DataBaseQuery;
import il.ac.hit.salarycalculator.model.UserUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserUtilsTest {
    UserUtils userUtils = new UserUtils();

    @Before
    public void setUp() throws Exception {
        userUtils.setModel(DataBaseQuery.getInstance());
        userUtils.initialize();
    }

    @Test
    public void checkIfUserExist() {
        assertFalse(userUtils.checkIfUserExist("test","test"));
    }

    @Test
    public void checkIfSettingExist() {
        assertFalse(userUtils.checkIfSettingExist("test"));
    }
}