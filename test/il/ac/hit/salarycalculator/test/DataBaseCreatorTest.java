package il.ac.hit.salarycalculator.test;

import il.ac.hit.salarycalculator.model.DataBaseCreator;
import org.junit.Test;

import static org.junit.Assert.*;

public class DataBaseCreatorTest
{
    @Test
    public void getInstance() {
        assertNotNull(DataBaseCreator.getInstance());
    }

    @Test
    public void getStatement() {
        assertNotNull(DataBaseCreator.getInstance().getStatement());
    }

    @Test
    public void getConnection() {
        assertNotNull(DataBaseCreator.getInstance().getConnection());
    }
}